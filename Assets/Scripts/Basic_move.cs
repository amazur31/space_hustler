﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Basic_move : MonoBehaviour
{
    private Rigidbody2D rb;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    public float maxVelocity = 3;
    public float rotationSpeed = 1;
    void Update()
    {
        float xAxis = Input.GetAxis("Horizontal");
        float yAxis = Input.GetAxis("Vertical");

        ForwardThrust(yAxis);
        Rotate(transform, xAxis * -rotationSpeed);
    }
    private void LimitVelocity()
    {
        float x = Mathf.Clamp(rb.velocity.x, -maxVelocity, maxVelocity);
        float y = Mathf.Clamp(rb.velocity.y, -maxVelocity, maxVelocity);

        rb.velocity = new Vector2(x, y);
    }
    private void ForwardThrust(float amount)
    {
        Vector2 force = transform.up * amount;

        rb.AddForce(force);
    }
    private void Rotate(Transform t, float amount)
    {
        t.Rotate(0, 0, amount);
    }
}